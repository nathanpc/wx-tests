#!/usr/bin/env python

import wx

class MainWindow(wx.Frame):
	# Deriving a new class from wx.Frame
	def __init__(self, parent, title):
		wx.Frame.__init__(self, parent, title = title, size = (200, 100))
		
		self.control = wx.TextCtrl(self, style = wx.TE_MULTILINE)
		self.CreateStatusBar()  # Status bar at the bottom
		
		# Setup the menu
		filemenu = wx.Menu()
		
		# wx.ID_ABOUT and wx.ID_EXIT are standard IDs provided by wxWidgets
		menuAbout = filemenu.Append(wx.ID_ABOUT, "&About", "Information about this app")
		filemenu.AppendSeparator()
		menuExit = filemenu.Append(wx.ID_EXIT, "E&xit", "Exit the app")
		
		# Create the menubar
		menuBar = wx.MenuBar()
		menuBar.Append(filemenu, "&File")  # Appends the File menu
		self.SetMenuBar(menuBar)
		
		# Set events
		self.Bind(wx.EVT_MENU, self.OnAbout, menuAbout)
		self.Bind(wx.EVT_MENU, self.OnExit, menuExit)
		
		self.Show(True)
	
	def OnAbout(self, e):
		dlg = wx.MessageDialog(self, "A small text editor", "About Sample Editor", wx.OK)
		
		dlg.ShowModal()
		dlg.Destroy()
	
	def OnExit(self, e):
		self.Close(True)

app = wx.App(False)
frame = MainWindow(None, "Simple Editor")

app.MainLoop()